import { Navigation } from 'react-native-navigation';
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import { HOME } from './screens';

import HomeScreen from './screens/Home';
import { withReduxProvider } from './store';

const Screens = new Map<string, React.FC<any>>();

Screens.set(HOME, HomeScreen);


// Register screens
Screens.forEach((C, key) => {
    Navigation.registerComponent(
        key,
        () => gestureHandlerRootHOC(withReduxProvider(C)),
        () => C,
    );
});

// Here some global listeners could be placed
// ...

export const startApp = () => {
    Navigation.setRoot({
        root: {
            component: {
                name: HOME,
                id: HOME
            }
        },
    });
};
