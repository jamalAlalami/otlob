import React, { useCallback } from 'react';
import {
    SafeAreaView,
    FlatList,
    KeyboardAvoidingView,
    Dimensions,
    Platform,
    Alert,
} from 'react-native';
import { Navigation, NavigationFunctionComponent } from 'react-native-navigation';
import { WebView } from 'react-native-webview';


interface Props { }

const Home: NavigationFunctionComponent<Props> = ({
    componentId,
}): JSX.Element => {
    // Redux Hooks
    // ===========
    // selectors
    return (
        <SafeAreaView style={{ flex: 1, }}>
            <WebView source={{ uri: 'https://r.otlob.io/mac' }} />
        </SafeAreaView>
    );
};

export default Home;
